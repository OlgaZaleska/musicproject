<%@page import="java.util.TreeMap"%>
<%@page import="objectstructures.Album"%>

<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="objectstructures.Band"%>

<%
Map<Integer, Album> allAlbums = (Map<Integer, Album>) request.getAttribute("albumsList");
if (allAlbums == null)
	allAlbums = new TreeMap<Integer, Album>();
Map<Integer, Band> allBands = (Map<Integer, Band>) request.getAttribute("bandList");
if (allBands == null)
	allBands = new TreeMap<Integer, Band>();
%>
<jsp:include page="headerTemplate.jsp" />
<body>
	<div class="container">
		<form id="albumsForm" action="/MusicWebProjectOlga/Albums"
			method="POST">
			<input hidden id="deleteAlbumId" name="deleteAlbumId"> 
			<input hidden id="albumRenamed" name="albumRenamed">
			<ul class="list-group" style="width: 50%; word-wrap: break-word">
				<%
				Iterator<Entry<Integer, Album>> iteratorAlb = allAlbums.entrySet().iterator();
				while (iteratorAlb.hasNext()) {
					Entry<Integer, Album> album = iteratorAlb.next();
				%>
				<li class="list-group-item">
					<div class="row">
					<div style="display: contents"
							id="displayAlbumTitle [ <%=album.getKey()%>]">
							<a
							 href="/MusicWebProjectOlga/Album?albumId=<%=album.getKey()%>">
							<%=album.getValue().getTitle()%>
						</a>
						</div>
						<div style="display: contents"
							id="displayAlbumBand [ <%=album.getKey()%>]">
							
							<%=album.getValue().getBandName()%>
					
						</div>
				 <input hidden id="renameTitle[<%=album.getKey()%>]"
					name="renameTitle[<%=album.getKey()%>]"
					value="<%=album.getValue().getTitle()%>">
				<select style ="display: none"  id="bandToChange" name="bandToChange">
					<option id = "0l" value = ""> </option>
				<%
					Iterator<Entry<Integer, Band>> iteratorBandChange = allBands.entrySet().iterator();
					while (iteratorBandChange.hasNext()) {
						Entry<Integer, Band> bandCh = iteratorBandChange.next();
				%>
					<option id="displayBandTitleCh[<%=bandCh.getKey()%>]" value ="<%=bandCh.getKey()%>">
						<%=bandCh.getValue().getName()%>
					</option>
				<%
				}
				%>
				</select>
					
					<button type="button"
						onclick="document.getElementById('deleteAlbumId').value = <%=album.getKey()%>;
						document.getElementById('albumsForm').submit()"
						class="fa fa-trash icon-button"></button>
					<button type="button" id="renameButton[<%=album.getKey()%>]"
						onclick="onRename(<%=album.getKey()%>)" 
						class="btn fa-trash:before" style="text-align: right;">Rename</button>
					<button type="button" class="btn"
						id="saveTitle[<%=album.getKey()%>]" style="display: none"
						onclick="document.getElementById('albumRenamed').value = <%=album.getKey()%>;
						document.getElementById('albumsForm').submit()">Save</button>
					<button type="button" class="btn"
						onclick="onCancel(<%=album.getKey()%>)"
						id="cancelInput[<%=album.getKey()%>]" style="display: none">Cancel</button>
	</div>
	</li>
	<%
	}
	%>
	</ul>
	<div class="row">
		<label for="albumTitle">Album title / Band</label> 
		<input
			id="albumTitle" name="albumTitle">
			
		<select id="bandToAdd" name="bandToAdd">
		<option id = "0" value = "">
		<%

				Iterator<Entry<Integer, Band>> iteratorBand = allBands.entrySet().iterator();
				while (iteratorBand.hasNext()) {
					Entry<Integer, Band> band = iteratorBand.next();
				%>
				<option id="displayBandTitle[<%=band.getKey()%>]" value ="<%=band.getKey()%>">
				<%=band.getValue().getName()%>
				</option>
				<%
				}
				%>
			</select>
		<button name="insertNewAlbum" type="submit" class="btn btn-success">Insert</button>
	</div>
	
	</form>
	<%
	if (request.getAttribute("albumInserted") != null && (boolean) request.getAttribute("albumInserted") == true) {
	%>
	<div>
		<h1 style="color: green">Entry is entered!</h1>
	</div>
	<%
	}
	%>
	
	</div>
	<script>
	function onRename(key){
		document.getElementById("renameTitle[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("bandToChange[".concat(key).concat("]")).style.visibility='visible';
		document.getElementById("displayAlbumTitle[".concat(key).concat("]")).style.display='none';
		document.getElementById("saveTitle[".concat(key).concat("]")).style.display = 'inline-block';
		document.getElementById("renameButton[".concat(key).concat("]")).style.display='none';
		document.getElementById("cancelInput[".concat(key).concat("]")).style.display = 'inline-block'
	}
	
	function onCancel(key){
		document.getElementById("renameTitle[".concat(key).concat("]")).style.display='none';
		document.getElementById("displayAlbumTitle[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("saveTitle[".concat(key).concat("]")).style.display = 'none';
		document.getElementById("renameButton[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("cancelInput[".concat(key).concat("]")).style.display = 'none'
	}
	
	</script>
</body>
</html>