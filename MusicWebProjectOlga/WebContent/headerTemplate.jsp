<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html lang="en">
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="styles/css/bootstrap.min.css">
<link rel="stylesheet" href="styles/css/bootstrap.css">
<link rel="stylesheet" href="styles/css/style.css">
<link
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet">
<link rel="shortcut icon" type="image/jpg" href="files/logos/music.jpg">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<meta charset="ISO-8859-1">
<title>Music</title>
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
		
			<ul class="nav navbar-nav">
				<li><a class="nav-link" href="/MusicWebProjectOlga/AllBands">Bands</a></li>
			</ul>
			
			<ul class="nav navbar-nav">
				<li><a class="nav-link" href="/MusicWebProjectOlga/Musicians">Musicians</a></li>
			</ul>
			
			<ul class="nav navbar-nav">
				<li class="nav-item"><a class="nav-link" href="/MusicWebProjectOlga/Albums">Albums</a></li></ul>
				
				<ul class="nav navbar-nav">
				<li class="nav-item"><a class="nav-link" href="/MusicWebProjectOlga/Songs">Songs</a></li></ul>
						</div>
	</nav>
</body>
</html>