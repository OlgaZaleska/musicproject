<%@ page contentType="text/html; charset=UTF-8"%>

<%@page import="java.util.TreeMap"%>
<%@page import="objectstructures.Song"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Iterator"%>
<%@page import="objectstructures.Band"%>


<%
Map<Integer, Song> allSongs = (Map<Integer, Song>) request.getAttribute("songsList");
if (allSongs == null)
	allSongs = new TreeMap<Integer, Song>();

Map<Integer, String> songsInAlbum = (Map<Integer, String>) request.getAttribute("songsInAlbum");

%>

<jsp:include page="headerTemplate.jsp" />
<body>
	<div class="container">
		<form id="albumForm"
			action="/MusicWebProjectOlga/Album?<%=request.getQueryString()%>"
			method="POST">
			<input hidden id="entryToDelete" name="entryToDelete">
						<h2>Songs in album <%=request.getAttribute("albumTitleAtr")%>:</h2>

			<ul class="list-group"
				style="width: 30%; word-wrap: break-word; text-align: left:;">
				<%
				Iterator<Entry<Integer, String>> iteratorBand = songsInAlbum.entrySet().iterator();
				while (iteratorBand.hasNext()) {
					Entry<Integer, String> songInAlbum = iteratorBand.next();
				%>
				<li class="list-group-item">
					<div class="row">
						<div style="display: contents"
							id="displaySongInName[<%=songInAlbum.getKey()%>]">
							<%=songInAlbum.getValue()%>
						</div>
						<button type="button" class="fa fa-trash icon-button"
							onclick="document.getElementById('entryToDelete').value = <%=songInAlbum.getKey()%>;
						document.getElementById('albumForm').submit()"></button>
					</div>
				</li>
				<%
				}
				%>
			</ul>


			<input hidden id="songToAdd" name="songToAdd">
			<h2>All songs list:</h2>
			<ul class="list-group"
				style="width: 30%; word-wrap: break-word; text-align: right:;">
				<%
				Iterator<Entry<Integer, Song>> iteratorS = allSongs.entrySet().iterator();
				while (iteratorS.hasNext()) {
					Entry<Integer, Song> song = iteratorS.next();
				%>
				<li class="list-group-item">
					<div class="row">
						<div style="display: contents"
							id="displaySongTitle[<%=song.getKey()%>]">
							<%=song.getValue().getTitle()%></div>
						<button type="button" class="fa fa-plus-square"
							onclick="document.getElementById('songToAdd').value = <%=song.getKey()%>;
						document.getElementById('albumForm').submit()"></button>
					</div>
				</li>
				<%
				}
				%>
			</ul>



		</form>
	</div>
</body>
