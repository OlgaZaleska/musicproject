<%@page import="java.util.TreeMap"%>
<%@page import="objectstructures.Musician"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Iterator"%>
<jsp:include page="headerTemplate.jsp" />

<%
Map<Integer, Musician> allMusicians = (Map<Integer, Musician>) request.getAttribute("musiciansList");
if (allMusicians == null)
	allMusicians = new TreeMap<Integer, Musician>();
%>

<body>
	<div class="container">
		<form id="musicianForm" action="/MusicWebProjectOlga/Musicians" method="POST">
			<input hidden id="deleteMusicianId" name="deleteMusicianId"> <input
				hidden id="musicianChanged" name="musicianChanged">
			<ul class="list-group" style="width: 70%; word-wrap: break-word">
				<%
				Iterator<Entry<Integer, Musician>> iterator = allMusicians.entrySet().iterator();
				while (iterator.hasNext()) {
					Entry<Integer, Musician> musician = iterator.next();
				%>
				<li class="list-group-item">
					<div class="row">
						<div style="display: contents"
							id="displayMusicianName[<%=musician.getKey()%>]">
							<%=musician.getValue().getName()%>
						</div>
						<div style="display: contents"
							id="displayMusicianSurname[<%=musician.getKey()%>]">
							<%=musician.getValue().getSurname()%>
						</div>
						<div style="display: contents"
							id="displayMusicianDate[<%=musician.getKey()%>]">
							<%=musician.getValue().getBirthDate()%>
						</div>
						<input hidden id="renameName[<%=musician.getKey()%>]"
							name="renameName[<%=musician.getKey()%>]"
							value="<%=musician.getValue().getName()%>"> <input hidden
							id="renameSurname[<%=musician.getKey()%>]"
							name="renameSurname[<%=musician.getKey()%>]"
							value="<%=musician.getValue().getSurname()%>"> <input hidden
							id="changeDate[<%=musician.getKey()%>]"
							name="changeDate[<%=musician.getKey()%>]" type="date"
							value="<%=musician.getValue().getBirthDate()%>">
						<button type="button"
							onclick="document.getElementById('deleteMusicianId').value = <%=musician.getKey()%>;
						document.getElementById('musicianForm').submit()"
							class="fa fa-trash icon-button"></button>
						<button type="button" id="changeButton[<%=musician.getKey()%>]"
							onclick="onChange(<%=musician.getKey()%>)" class="btn"
							style="text-align: right;">Change</button>
						<button type="button" class="btn"
							id="saveMusician[<%=musician.getKey()%>]" style="display: none"
							onclick="document.getElementById('musicianChanged').value = <%=musician.getKey()%>;
						document.getElementById('musicianForm').submit()">Save</button>
						<button type="button" class="btn"
							onclick="onCancel(<%=musician.getKey()%>)"
							id="cancelInput[<%=musician.getKey()%>]" style="display: none">Cancel</button>
					</div>
				</li>
				<%
				}
				%>
			</ul>

			<div class="row">
				<label for="insertNewMusician"> Musician name/surname/birth date
				</label> <input id="musicianNameInsert" name="musicianNameInsert"> <input
					id="musicianSurnameInsert" name="musicianSurnameInsert"> <input
					id="musicianDateInsert" name="musicianDateInsert" type="date">
				<button name="insertNewMusician" type="submit" class="btn btn-success">Insert</button>
			</div>
		</form>
	</div>
	<script>
	function onChange(key){
		document.getElementById("renameName[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("renameSurname[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("changeDate[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("displayMusicianName[".concat(key).concat("]")).style.display='none';
		document.getElementById("displayMusicianSurname[".concat(key).concat("]")).style.display='none';
		document.getElementById("displayMusicianDate[".concat(key).concat("]")).style.display='none';
		document.getElementById("saveMusician[".concat(key).concat("]")).style.display = 'inline-block';
		document.getElementById("changeButton[".concat(key).concat("]")).style.display='none';
		document.getElementById("cancelInput[".concat(key).concat("]")).style.display = 'inline-block'
	}
	
	function onCancel(key){
		document.getElementById("renameName[".concat(key).concat("]")).style.display='none';
		document.getElementById("renameSurname[".concat(key).concat("]")).style.display='none';
		document.getElementById("changeDate[".concat(key).concat("]")).style.display='none';
		document.getElementById("displayMusicianName[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("displayMusicianSurname[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("displayMusicianDate[".concat(key).concat("]")).style.display='inline-block';
	    document.getElementById("saveMusician[".concat(key).concat("]")).style.display = 'none';
		document.getElementById("changeButton[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("cancelInput[".concat(key).concat("]")).style.display = 'none'
	}
	
	</script>
</body>