<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="objectstructures.Band"%>
<%
Map<Integer, Band> allBands = (Map<Integer, Band>) request.getAttribute("bandList");
if (allBands == null)

        allBands = new TreeMap<Integer, Band>();
%>

<jsp:include page="headerTemplate.jsp" />

<body>
	<div class="container">

		<form id="bandForm" action="/MusicWebProjectOlga/AllBands "
			method="POST">
			<input hidden id="deleteBandId" name="deleteBandId"> <input
				hidden id="bandRenamed" name="bandRenamed">
			<ul class="list-group" style="width: 50%; word-wrap: break-word">

				<%
				Iterator<Entry<Integer, Band>> iterator = allBands.entrySet().iterator();

				while (iterator.hasNext()) {
					Entry<Integer, Band> band = iterator.next();
				%>
				<li class="list-group-item">
					<div class="row">
					<a
													id="displayBandName[<%=band.getKey()%>]" 
						 	href="/MusicWebProjectOlga/Band?bandId=<%=band.getKey()%>">
							<%=band.getValue().getName()%>
						</a> 
						<input hidden id="rename[<%=band.getKey()%>]"
							name="rename[<%=band.getKey()%>]" value="<%=band.getValue().getName()%>">
						<button type="button"
							onclick="document.getElementById('deleteBandId').value = <%=band.getKey()%>;
						document.getElementById('bandForm').submit()"
							class="fa fa-trash icon-button"></button>
						<button type="button" id="renameButton[<%=band.getKey()%>]"
							onclick="onRename(<%=band.getKey()%>)"
							class="btn fa-trash:before" style="text-align: right;">
							Rename</button>
						<button type="button" class="btn"
							id="saveName[<%=band.getKey()%>]" style="display: none"
							onclick="document.getElementById('bandRenamed').value = <%=band.getKey()%>;
						document.getElementById('bandForm').submit()">Save</button>
						<button type="button" class="btn"
							onclick="onCancel(<%=band.getKey()%>)"
							id="cancelInput[<%=band.getKey()%>]" style="display: none">Cancel</button>
					</div>
				</li>

				<%
				}
				%>
			</ul>
			<div class="row">
				<label for="bandName">Band name</label> <input id="bandName"
					name="bandName">
				<button name="insertNewBand" type="submit" class="btn-success">Insert</button>
			</div>
		</form>
		<%
		if (request.getAttribute("bandInserted") != null && (boolean) request.getAttribute("bandInserted") == true) {
		%>

		<div>
			<h1 style="color: green">Entry is entered</h1>
		</div>
		<%
		}
		%>
	</div>
	<script>
	function onRename(key){
		document.getElementById("rename[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("displayBandName[".concat(key).concat("]")).style.display='none';
		document.getElementById("saveName[".concat(key).concat("]")).style.display = 'inline-block';
		document.getElementById("renameButton[".concat(key).concat("]")).style.display='none';
		document.getElementById("cancelInput[".concat(key).concat("]")).style.display = 'inline-block'
	}
	
	function onCancel(key){
		document.getElementById("rename[".concat(key).concat("]")).style.display='none';
		document.getElementById("displayBandName[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("saveName[".concat(key).concat("]")).style.display = 'none';
		document.getElementById("renameButton[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("cancelInput[".concat(key).concat("]")).style.display = 'none'
	}
	
	</script>
</body>
</html>