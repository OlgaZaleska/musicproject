<%@page import="java.util.TreeMap"%>
<%@page import="objectstructures.Song"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Iterator"%>
<jsp:include page="headerTemplate.jsp" />

<%
Map<Integer, Song> allSongs = (Map<Integer, Song>) request.getAttribute("songsList");
if (allSongs == null)
	allSongs = new TreeMap<Integer, Song>();
%>

<body>
	<div class="container">
		<form id="songForm" action="/MusicWebProjectOlga/Songs" method="POST">
			<input hidden id="deleteSongId" name="deleteSongId"> <input
				hidden id="songChanged" name="songChanged">
				<label>Song title / Album title</label>
			<ul class="list-group" style="width: 70%; word-wrap: break-word">
				<%
				Iterator<Entry<Integer, Song>> iterator = allSongs.entrySet().iterator();
				while (iterator.hasNext()) {
					Entry<Integer, Song> song = iterator.next();
				%>
				<li class="list-group-item">
					<div class="row">
						<div style="display: contents;  font-weight: bold; color:blue; "
							id="displaySongName[<%=song.getKey()%>]">
							<%=song.getValue().getTitle()%>
						</div>
<div style="display: contents"
                                                        id="displayAlbum[<%=song.getKey()%>]">
                                                        <%=song.getValue().getAlbumName()%>
                                                        </div>
						<input hidden id="renameTitle[<%=song.getKey()%>]"
							name="renameTitle[<%=song.getKey()%>]"
							value="<%=song.getValue().getTitle()%>">
						<button type="button"
							onclick="document.getElementById('deleteSongId').value = <%=song.getKey()%>;
						document.getElementById('songForm').submit()"
							class="fa fa-trash icon-button"></button>
						<button type="button" id="changeButton[<%=song.getKey()%>]"
							onclick="onChange(<%=song.getKey()%>)" class="btn"
							style="text-align: right;">Change</button>
						<button type="button" class="btn"
							id="saveSong[<%=song.getKey()%>]" style="display: none"
							onclick="document.getElementById('songChanged').value = <%=song.getKey()%>;
						document.getElementById('songForm').submit()">Save</button>
						<button type="button" class="btn"
							onclick="onCancel(<%=song.getKey()%>)"
							id="cancelInput[<%=song.getKey()%>]" style="display: none">Cancel</button>
					</div>
				</li>
				<%
				}
				%>
			</ul>

			<div class="row">
				<label for="insertNewSong"> Song title</label> <input
					id="songTitleInsert" name="songTitleInsert">
				<button name="insertNewSong" type="submit" class="btn btn-success">Insert</button>
			</div>
		</form>
	</div>
	<script>
	function onChange(key){
		document.getElementById("renameTitle[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("displaySongTitle[".concat(key).concat("]")).style.display='none';
		document.getElementById("saveSong[".concat(key).concat("]")).style.display = 'inline-block';
		document.getElementById("changeButton[".concat(key).concat("]")).style.display='none';
		document.getElementById("cancelInput[".concat(key).concat("]")).style.display = 'inline-block'
	}
	
	function onCancel(key){
		document.getElementById("renameTitle[".concat(key).concat("]")).style.display='none';
		document.getElementById("displaySongTitle[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("saveSong[".concat(key).concat("]")).style.display = 'none';
		document.getElementById("changeButton[".concat(key).concat("]")).style.display='inline-block';
		document.getElementById("cancelInput[".concat(key).concat("]")).style.display = 'none'
	}
	
	</script>
</body>