<%@ page contentType="text/html; charset=UTF-8"%>

<%@page import="java.util.TreeMap"%>
<%@page import="objectstructures.Musician"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Iterator"%>

<%
Map<Integer, Musician> allMusicians = (Map<Integer, Musician>) request.getAttribute("musiciansList");
if (allMusicians == null)
	allMusicians = new TreeMap<Integer, Musician>();

Map<Integer, String> musiciansInBand = (Map<Integer, String>) request.getAttribute("musiciansInBand");
%>

<jsp:include page="headerTemplate.jsp" />
<body>
	<div class="container">
		<form id="bandForm"
			action="/MusicWebProjectOlga/Band?<%=request.getQueryString()%>"
			method="POST">

			<input hidden id="entryToDelete" name="entryToDelete">
			<h2>Musicians in band:</h2>
			<ul class="list-group"
				style="width: 30%; word-wrap: break-word; text-align: left: ;">
				<%
				Iterator<Entry<Integer, String>> iteratorBand = musiciansInBand.entrySet().iterator();
				while (iteratorBand.hasNext()) {
					Entry<Integer, String> musicianInBand = iteratorBand.next();
				%>
				<li class="list-group-item">
					<div class="row">
						<div style="display: contents"
							id="displayMusicianInName[<%=musicianInBand.getKey()%>]">
							<%=musicianInBand.getValue()%>
						</div>
						<button type="button" class="fa fa-trash icon-button"
							onclick="document.getElementById('entryToDelete').value = <%=musicianInBand.getKey()%>;
						document.getElementById('bandForm').submit()"></button>
					</div>
				</li>
				<%
				}
				%>
			</ul>

			<input hidden id="musicianToAdd" name="musicianToAdd">
			<h2>All musicians list:</h2>
			<ul class="list-group"
				style="width: 30%; word-wrap: break-word; text-align: right: ;">
				<%
				Iterator<Entry<Integer, Musician>> iterator = allMusicians.entrySet().iterator();
				while (iterator.hasNext()) {
					Entry<Integer, Musician> musician = iterator.next();
				%>
				<li class="list-group-item">
					<div class="row">
						<div style="display: contents"
							id="displayMusicianName[<%=musician.getKey()%>]">
							<%=musician.getValue().getName()%></div>
							<div style="display: contents"
								id="displayMusicianSurname[<%=musician.getKey()%>]">
								<%=musician.getValue().getSurname()%>
							</div>
							<button type="button" class="fa fa-user-plus"
								onclick="document.getElementById('musicianToAdd').value = <%=musician.getKey()%>;
						document.getElementById('bandForm').submit()"></button>
						</div>
				</li>
				<%
				}
				%>			
			</ul>
		</form>
	</div>
</body>
