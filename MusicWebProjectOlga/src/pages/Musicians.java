package pages;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import database.DatabaseManager;
import database.MusiciansDatabaseManager;

/**
 * Servlet implementation class Musicians
 */
@WebServlet("/Musicians")
public class Musicians extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Musicians() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			DatabaseManager musicianManager = new MusiciansDatabaseManager();
			if(request.getParameter("insertNewMusician") !=null)
				musicianManager.addNewEntry(request, response);
			
			else if (request.getParameter("deleteMusicianId") != null && request.getParameter("deleteMusicianId") != "")
				musicianManager.deleteEntry(Integer.parseInt(request.getParameter("deleteMusicianId")));
			else if (request.getParameter("musicianChanged") != null && request.getParameter("musicianChanged") != "")
				musicianManager.changeEntry(request);

					
			musicianManager.setAllEntries(request);
			DatabaseManager.closeConnection();
			
		} catch ( Exception e) {
			e.printStackTrace();
		}
		RequestDispatcher view = request.getRequestDispatcher("Musicians.jsp");
		view.forward(request, response);
	}

}
