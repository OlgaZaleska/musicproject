package pages;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.AlbumsDatabaseManager;
import database.BandDatabaseManager;
import database.DatabaseManager;

/**
 * Servlet implementation class Albums
 */
@WebServlet("/Albums")
public class Albums extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Albums() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			DatabaseManager albumsDbManager = new AlbumsDatabaseManager();
			BandDatabaseManager bandsDbManager = new BandDatabaseManager();
			if (request.getParameter("insertNewAlbum") != null)
				albumsDbManager.addNewEntry(request, response);
			else if (request.getParameter("deleteAlbumId") != null && request.getParameter("deleteAlbumId") != "")
				albumsDbManager.deleteEntry(Integer.parseInt(request.getParameter("deleteAlbumId")));
			else if (request.getParameter("albumRenamed") != null && request.getParameter("albumRenamed") != "")
				albumsDbManager.changeEntry(request);
			else if (request.getParameter("bandToAdd") != null && request.getParameter("bandToAdd") != "")
				;
			else if (request.getParameter("bandToChange") != null && request.getParameter("bandToChange") != "")
				;
			albumsDbManager.setAllEntries(request);
			bandsDbManager.setAllEntries(request);
			
			DatabaseManager.closeConnection();

		} catch (Exception e) {
			e.printStackTrace();
		}

		RequestDispatcher view = request.getRequestDispatcher("Albums.jsp");
		view.forward(request, response);
	}

}
