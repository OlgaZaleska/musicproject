package pages;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import database.DatabaseManager;
import database.SongsDatabaseManager;

/**
 * Servlet implementation class Songs
 */
@WebServlet("/Songs")
public class Songs extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Songs() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			DatabaseManager musicianManager = new SongsDatabaseManager();
			if(request.getParameter("insertNewSong") !=null)
				musicianManager.addNewEntry(request, response);
			
			else if (request.getParameter("deleteSongId") != null && request.getParameter("deleteSongId") != "")
				musicianManager.deleteEntry(Integer.parseInt(request.getParameter("deleteSongId")));
			else if (request.getParameter("musicianChanged") != null && request.getParameter("musicianChanged") != "")
				musicianManager.changeEntry(request);

					
			musicianManager.setAllEntries(request);
			DatabaseManager.closeConnection();
			
		} catch ( Exception e) {
			e.printStackTrace();
		}
		RequestDispatcher view = request.getRequestDispatcher("Songs.jsp");
		view.forward(request, response);
	}

}

