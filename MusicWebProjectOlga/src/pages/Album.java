package pages;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DatabaseManager;
import database.AlbumsDatabaseManager;
import database.SongsDatabaseManager;

/**
 * Servlet implementation class Album
 */
@WebServlet("/Album")
public class Album extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Album() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	private int getAlbumIdFromQuery(String query) {

		String[] queries = query.split("&");
		for (int i = 0; i < queries.length; i++) {
			String[] queryParametr = queries[i].split("=");
			if (queryParametr[0].equals("albumId"))
				return Integer.parseInt(queryParametr[1]);
		}
		return 0;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {

			int albumId = this.getAlbumIdFromQuery(request.getQueryString());
			SongsDatabaseManager songsDbManager = new SongsDatabaseManager();
			AlbumsDatabaseManager albumDBManager = new AlbumsDatabaseManager();
			request.setAttribute("albumTitleAtr", albumDBManager.getAlbumTitle(albumId));

			if (request.getParameter("songToAdd") != null && !request.getParameter("songToAdd").equals(""))
				albumDBManager.addSongToAlbum(Integer.parseInt(request.getParameter("songToAdd")), albumId);

			else if (request.getParameter("entryToDelete") != null && !request.getParameter("entryToDelete").equals(""))
				albumDBManager.deleteSongAlbumEntry(Integer.parseInt(request.getParameter("entryToDelete")));

			else if (request.getParameter("entryToDelete") != null && !request.getParameter("entryToDelete").equals(""))
				albumDBManager.deleteSongAlbumEntry(Integer.parseInt(request.getParameter("entryToDelete")));
			songsDbManager.setAllEntries(request);

			songsDbManager.setAllEntries(request);

			albumDBManager.setAllSongsForAlbum(albumId, request);

			DatabaseManager.closeConnection();

		} catch (Exception e) {
			e.printStackTrace();
		}

		RequestDispatcher view = request.getRequestDispatcher("Album.jsp");
		view.forward(request, response);
	}
}