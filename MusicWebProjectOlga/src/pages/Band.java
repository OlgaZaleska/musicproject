package pages;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DatabaseManager;
import database.BandDatabaseManager;
import database.MusiciansDatabaseManager;

/**
 * Servlet implementation class Band
 */
@WebServlet("/Band")
public class Band extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Band() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	private int getBandIdFromQuery(String query) {

		String[] queries = query.split("&");
		for (int i = 0; i < queries.length; i++) {
			String[] queryParametr = queries[i].split("=");
			if (queryParametr[0].equals("bandId"))
				return Integer.parseInt(queryParametr[1]);
		}
		return 0;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			int bandId = this.getBandIdFromQuery(request.getQueryString());
			MusiciansDatabaseManager musiciansDbManager = new MusiciansDatabaseManager();
			BandDatabaseManager bandDBManager = new BandDatabaseManager();

			if (request.getParameter("musicianToAdd") != null && !request.getParameter("musicianToAdd").equals(""))
				bandDBManager.addMusicianToBand(Integer.parseInt(request.getParameter("musicianToAdd")), bandId);

			else if (request.getParameter("entryToDelete") != null && !request.getParameter("entryToDelete").equals(""))
				bandDBManager.deleteMusicianBandEntry(Integer.parseInt(request.getParameter("entryToDelete")));

			musiciansDbManager.setAllEntries(request);

			bandDBManager.setAllMusiciansForBand(bandId, request);
			DatabaseManager.closeConnection();

		} catch (Exception e) {
			e.printStackTrace();
		}

		RequestDispatcher view = request.getRequestDispatcher("Band.jsp");
		view.forward(request, response);
	}
}
