package pages;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.BandDatabaseManager;
import database.DatabaseManager;

/**
 * Servlet implementation class AllBands
 */
@WebServlet("/AllBands")
public class AllBands extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AllBands() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			DatabaseManager bandDbManager = new BandDatabaseManager();

			if (request.getParameter("insertNewBand") != null)
				bandDbManager.addNewEntry(request, response);
			else if (request.getParameter("deleteBandId") != null && request.getParameter("deleteBandId") != "")
				bandDbManager.deleteEntry(Integer.parseInt(request.getParameter("deleteBandId")));
			else if (request.getParameter("bandRenamed") != null && request.getParameter("bandRenamed") != "")
				bandDbManager.changeEntry(request);

			bandDbManager.setAllEntries(request);
			DatabaseManager.closeConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		RequestDispatcher view = request.getRequestDispatcher("AllBands.jsp");
		view.forward(request, response);
	}

}
