package objectstructures;

public class Musician {
	private String name, surname, birthDate;

	
	public Musician(String name, String surname, String birthDate) {
		super();
		this.name = name;
		this.surname = surname;
		this.birthDate = birthDate;
	}


	public String getName() {
		return name;
	}


	public String getSurname() {
		return surname;
	}


	public String getBirthDate() {
		return birthDate;
	}
	

}
