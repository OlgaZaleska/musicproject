package objectstructures;

public class Album {
	private String title, bandName;
	

	public Album(String title) {
		this.title = title;
	}
	
	public Album(String title, String bandName) {
		super();
		this.title = title;
		this.bandName = bandName;
	}
	
	public Album() {
		title = "";
	}

	public String getTitle() {
		return title;
	}

	public String getBandName() {
		return bandName;
	}
	
	

}
