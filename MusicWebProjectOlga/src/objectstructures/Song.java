package objectstructures;

public class Song {
	private String Title;
	private String AlbumName;
	private String bandName;

	public Song(String title) {
		this.Title = title;
	}
	public Song(String title, String AlbumName) {
		this.Title = title;
		this.AlbumName = AlbumName;
	}
	public Song(String title, String AlbumName, String bandName) {
		this.Title = title;
		this.AlbumName = AlbumName;
		this.bandName = bandName;
	}
	
	public String getTitle() {
		return Title;
	}
	public String getAlbumName() {
		return AlbumName;
	}
	public String getBandName() {
		return bandName;
	}
	public void setAlbumName(String AlbumName) {
		this.AlbumName = AlbumName;
	}

}
