package objectstructures;

public class Band {
private String name;

public Band(String name) {
	super();
	this.name = name;
}

public String getName() {
	return name;
}

}
