package database;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import objectstructures.Musician;

public class MusiciansDatabaseManager extends DatabaseManager {

	public MusiciansDatabaseManager() throws Exception {
		super();
	}

	@Override
	public void deleteEntry(int id) throws SQLException {
		PreparedStatement stmt = DatabaseManager.con.prepareStatement("DELETE FROM musicians WHERE ID = ?");
		stmt.setInt(1, id);
		stmt.execute();
	}

	private Date getDateSql(String dateInsert) {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yy-MM-dd");

		Date dateSql = null;
		try {
			dateSql = new Date(dateFormatter.parse(dateInsert).getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dateSql;
	}

	@Override
	public void changeEntry(HttpServletRequest request) throws SQLException {
		int musicianIdRename = Integer.parseInt(request.getParameter("musicianChanged"));
		String newName = request.getParameter("renameName[" + musicianIdRename + "]");
		String newSurname = request.getParameter("renameSurname[" + musicianIdRename + "]");
		String dateInput = request.getParameter("changeDate[" + musicianIdRename + "]");

		PreparedStatement stmt = DatabaseManager.con
				.prepareStatement("UPDATE musicians SET Name = ?,Surname = ?,Birth_Date = ? WHERE ID = ?");
		stmt.setString(1, newName);
		stmt.setString(2, newSurname);
		stmt.setDate(3, this.getDateSql(dateInput));
		stmt.setInt(4, musicianIdRename);
		stmt.executeUpdate();
	}

	@Override
	public void addNewEntry(HttpServletRequest request, HttpServletResponse response) throws Exception {
		PreparedStatement stmt = DatabaseManager.con
				.prepareStatement("INSERT INTO musicians (Name,Surname,Birth_Date) VALUES (?,?,?)");
		stmt.setString(1, request.getParameter("musicianNameInsert"));
		stmt.setString(2, request.getParameter("musicianSurnameInsert"));
		stmt.setDate(3, this.getDateSql(request.getParameter("musicianDateInsert")));
		stmt.execute();
		request.setAttribute("musicianInserted", true);

	}

	public void addMusicianToList(int musicianId, int bandId) throws Exception {
		PreparedStatement stmt = DatabaseManager.con
				.prepareStatement("INSERT INTO band_members (BandID, MusicianID VALUES (?,?)");
		stmt.setInt(1, bandId);
		stmt.setInt(2, musicianId);
		stmt.execute();

	}

	@Override
	public void setAllEntries(HttpServletRequest request) throws SQLException {

		Map<Integer, Musician> allMusicians = new TreeMap<Integer, Musician>();
		ResultSet allMusiciansResult = DatabaseManager.con.createStatement().executeQuery("SELECT * FROM musicians");

		while (allMusiciansResult.next())
			allMusicians.put(allMusiciansResult.getInt("ID"), new Musician(allMusiciansResult.getString("name"),
					allMusiciansResult.getString("surname"), allMusiciansResult.getDate("birth_date").toString()));

		request.setAttribute("musiciansList", allMusicians);

	}

}
