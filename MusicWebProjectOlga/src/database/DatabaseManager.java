package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class DatabaseManager implements DatabaseActions {
	protected static Connection con = null;

	public DatabaseManager() throws Exception {

		if (DatabaseManager.con != null && !DatabaseManager.con.isClosed())
			return;

		Class.forName("com.mysql.cj.jdbc.Driver");
		DatabaseManager.con = DriverManager.getConnection(
				"jdbc:mysql://127.0.0.1/music?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
				"root", "");
		DatabaseManager.con.setAutoCommit(false);
	}

	public static void closeConnection() throws SQLException {
		DatabaseManager.con.commit();
		DatabaseManager.con.close();
	}

}
