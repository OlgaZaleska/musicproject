package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import objectstructures.Band;

public class BandDatabaseManager extends DatabaseManager {

	public BandDatabaseManager() throws Exception {
		super();

	}

	@Override
	public void changeEntry(HttpServletRequest request) throws SQLException {
		int bandIdRename = Integer.parseInt(request.getParameter("bandRenamed"));
		String newBandName = request.getParameter("rename[" + bandIdRename + "]");

		PreparedStatement stmt = DatabaseManager.con.prepareStatement("UPDATE bands SET Name = ? WHERE ID = ?");
		stmt.setString(1, newBandName);
		stmt.setInt(2, bandIdRename);
		stmt.executeUpdate();

	}

	@Override
	public void deleteEntry(int bandId) throws SQLException {
		PreparedStatement stmt = DatabaseManager.con.prepareStatement("DELETE FROM bands WHERE ID = ?");
		stmt.setInt(1, bandId);
		stmt.execute();
	}

	@Override
	public void addNewEntry(HttpServletRequest request, HttpServletResponse response) throws Exception {
		PreparedStatement stmt = DatabaseManager.con.prepareStatement("INSERT INTO bands(Name) VALUES (?)");

		stmt.setString(1, request.getParameter("bandName"));
		stmt.execute();
		request.setAttribute("bandInserted", true);

	}

	public void setAllMusiciansForBand(int bandId, HttpServletRequest request) throws Exception {
		PreparedStatement stmt = this.con.prepareStatement(
				"SELECT * FROM band_members INNER JOIN musicians ON musicians.ID = band_members.MusicianID WHERE BandID = ?");

		stmt.setInt(1, bandId);
		ResultSet resultSet = stmt.executeQuery();
		Map<Integer, String> musicians = new TreeMap<Integer, String>();
		while (resultSet.next())
			musicians.put(resultSet.getInt("band_members.ID"),
					resultSet.getString("musicians.Name") + " " + resultSet.getString("musicians.Surname"));
		request.setAttribute("musiciansInBand", musicians);
	}

	@Override
	public void setAllEntries(HttpServletRequest request) throws SQLException {

		Map<Integer, Band> allBands = new TreeMap<Integer, Band>();
		ResultSet allBandsResult = DatabaseManager.con.createStatement().executeQuery("SELECT * FROM bands");

		while (allBandsResult.next())
			allBands.put(allBandsResult.getInt("ID"), new Band(allBandsResult.getString("name")));

		request.setAttribute("bandList", allBands);

	}

	public void addMusicianToBand(int musicianId, int bandId) throws Exception {

		PreparedStatement stmt = DatabaseManager.con
				.prepareStatement("INSERT INTO band_members (bandID,musicianID) VALUES (?,?)");
		stmt.setInt(1, bandId);
		stmt.setInt(2, musicianId);
		stmt.execute();

	}

	public void deleteMusicianBandEntry(int entryId) throws Exception {
		PreparedStatement stmt = DatabaseManager.con.prepareStatement("DELETE FROM band_members WHERE ID = ?");
		stmt.setInt(1, entryId);
		stmt.execute();

	}
}
