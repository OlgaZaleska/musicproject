package database;

	import java.sql.SQLException;

	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;

	public interface DatabaseActions {

		public void deleteEntry(int id) throws SQLException;

		public void setAllEntries(HttpServletRequest request) throws SQLException;

		public void changeEntry(HttpServletRequest request) throws SQLException;

		public void addNewEntry(HttpServletRequest request, HttpServletResponse response) throws Exception;

	}
