package database;

import objectstructures.Album;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AlbumsDatabaseManager extends DatabaseManager {

	public AlbumsDatabaseManager() throws Exception {
		super();

	}

	@Override
	public void deleteEntry(int albumId) throws SQLException {
		PreparedStatement stmt = DatabaseManager.con.prepareStatement("DELETE FROM albums WHERE ID = ?");
		stmt.setInt(1, albumId);
		stmt.execute();
	}

	@Override
	public void changeEntry(HttpServletRequest request) throws SQLException {
		int albumIdRename = Integer.parseInt(request.getParameter("albumRenamed"));
		String newAlbumTitle = request.getParameter("renameTitle[" + albumIdRename + "]");
		int bandID = Integer.parseInt(request.getParameter("bandToChange"));
		PreparedStatement stmt = DatabaseManager.con
				.prepareStatement("UPDATE albums SET Title = ?, BandID = ? WHERE ID = ?");
		stmt.setString(1, newAlbumTitle);
		stmt.setInt(2, bandID);
		stmt.setInt(3, albumIdRename);
		stmt.executeUpdate();
	}

	@Override
	public void addNewEntry(HttpServletRequest request, HttpServletResponse response) throws Exception {
		PreparedStatement stmt = DatabaseManager.con
				.prepareStatement("INSERT INTO albums (Title, BandID) VALUES (?,?)");
		int idBand = Integer.parseInt(request.getParameter("bandToAdd"));
		stmt.setInt(2, idBand);
		stmt.setString(1, request.getParameter("albumTitle"));

		stmt.execute();
		request.setAttribute("albumInserted", true);

	}

	public void setAllSongsForAlbum(int albumId, HttpServletRequest request) throws Exception {
		PreparedStatement stmt = AlbumsDatabaseManager.con.prepareStatement(
				"SELECT * FROM songs INNER JOIN albums ON albums.ID = songs.albumId WHERE albumId = ?");

		stmt.setInt(1, albumId);
		ResultSet resultSet = stmt.executeQuery();
		Map<Integer, String> songs = new TreeMap<Integer, String>();
		while (resultSet.next())
			songs.put(resultSet.getInt("songs.ID"), resultSet.getString("songs.Title"));
		request.setAttribute("songsInAlbum", songs);
	}

	@Override
	public void setAllEntries(HttpServletRequest request) throws SQLException {
		Map<Integer, Album> allAlbums = new TreeMap<Integer, Album>();
		ResultSet allAlbumsResult = DatabaseManager.con.createStatement()
				.executeQuery("SELECT * FROM albums INNER JOIN bands ON bands.ID = albums.BandID");
		ResultSet allAlbumsResult2 = DatabaseManager.con.createStatement()
				.executeQuery("SELECT * FROM albums Where albums.BandID is Null  ");
		while (allAlbumsResult.next()) {
			allAlbums.put(allAlbumsResult.getInt("albums.ID"),
					new Album(allAlbumsResult.getString("albums.Title"), allAlbumsResult.getString("bands.Name")));
		}

		while (allAlbumsResult2.next())
			allAlbums.put(allAlbumsResult2.getInt("albums.ID"), new Album(allAlbumsResult2.getString("albums.Title")));
		request.setAttribute("albumsList", allAlbums);
	}

	public void addSongToAlbum(int songId, int albumId) throws Exception {
		PreparedStatement stmt = DatabaseManager.con.prepareStatement("UPDATE songs SET albumId  =? WHERE ID = ?");
		stmt.setInt(1, albumId);
		stmt.setInt(2, songId);
		stmt.execute();

	}

		public void deleteSongAlbumEntry(int albumId) throws Exception {
		PreparedStatement stmt = DatabaseManager.con.prepareStatement("UPDATE songs SET albumID = NULL WHERE ID = ?");
		stmt.setInt(1, albumId);
		stmt.execute();

	}
		
		public String getAlbumTitle(int albumId) throws Exception
		{
			PreparedStatement stmt = AlbumsDatabaseManager.con.prepareStatement("SELECT title FROM albums WHERE Id = ?");

			stmt.setInt(1, albumId);
			ResultSet resultSet = stmt.executeQuery();
			while(resultSet.next())
				return resultSet.getString("title");
			return null;
		}
}
