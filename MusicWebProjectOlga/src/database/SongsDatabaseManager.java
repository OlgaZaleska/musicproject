package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import objectstructures.Song;

public class SongsDatabaseManager extends DatabaseManager {

	public SongsDatabaseManager() throws Exception {
		super();
	}

	@Override
	public void deleteEntry(int id) throws SQLException {
		PreparedStatement stmt = DatabaseManager.con.prepareStatement("DELETE FROM songs WHERE ID = ?");
		stmt.setInt(1, id);
		stmt.execute();
	}

	@Override
	public void changeEntry(HttpServletRequest request) throws SQLException {
		int songIdRename = Integer.parseInt(request.getParameter("songChanged"));
		String newTitle = request.getParameter("renameTitle[" + songIdRename + "]");

		PreparedStatement stmt = DatabaseManager.con.prepareStatement("UPDATE songs SET Title = ? WHERE ID = ?");
		stmt.setString(1, newTitle);
		stmt.setInt(2, songIdRename);
		stmt.executeUpdate();
	}

	@Override
	public void addNewEntry(HttpServletRequest request, HttpServletResponse response) throws Exception {
		PreparedStatement stmt = DatabaseManager.con.prepareStatement("INSERT INTO songs (title) VALUES (?)");
		stmt.setString(1, request.getParameter("songTitleInsert"));
		stmt.execute();
		request.setAttribute("songInserted", true);

	}

	public void addSongToList(int songId, int albumId) throws Exception {
		PreparedStatement stmt = DatabaseManager.con.prepareStatement("update songs SET (albumID) WHERE songID = ?");
		stmt.setInt(1, albumId);
		stmt.execute();

	}

	@Override
	public void setAllEntries(HttpServletRequest request) throws SQLException {

		Map<Integer, Song> allSongs = new TreeMap<Integer, Song>();
		ResultSet allSongsResult = DatabaseManager.con.createStatement().executeQuery(
				"SELECT * FROM songs INNER JOIN albums ON albums.ID = songs.AlbumID Where albumID is NOT NULL");
		ResultSet allSongsResult3 = DatabaseManager.con.createStatement().executeQuery(
				"SELECT * FROM songs INNER JOIN albums ON albums.ID = songs.AlbumID Where albumID is NOT NULL");

		ResultSet allSongsResult2 = DatabaseManager.con.createStatement()
				.executeQuery("SELECT * FROM songs Where albumID is NULL");
		while (allSongsResult.next())
			allSongs.put(allSongsResult.getInt("ID"),
					new Song(allSongsResult.getString("songs.title"), allSongsResult.getString("albums.title")));
		while (allSongsResult2.next())
			allSongs.put(allSongsResult2.getInt("ID"), new Song(allSongsResult2.getString("songs.title")));
		request.setAttribute("songsList", allSongs);
	}

}
